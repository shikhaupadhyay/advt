<html>
    <head>
    </head>
    <body>
	    <div class="container">
		  <?php
             include("includes/connection.php");
	         include("header.php");
          ?>
		</div>
		<!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Book Ad.</h2>
            <h3 class="section-subheading text-muted">Ad Booking at the Lowest Rates GUARANTEED !!!</h3>
          </div>
        </div>
        <div class="row ">
           <div class="col-md-4" id="news_box">
		        <i><img src ="img/sample-classified-text-ad-big.jpg"  width="80%" /></i>
                <h4 class="service-heading text-center">Classified Text Ad</h4><hr>
                
                     <p class="text-muted">&#9670; <b>Low-cost</b> text classifieds.</p>
                     <p class="text-muted">&#9670; Text ads that can be distinguished or highlighted with <b>coloured backgrounds.</b></p>
                     <p class="text-muted">&#9670; <b>Simple running ad</b>, can be enhanced with Tick, Border & Bold</p>
                 
     			<a class="btn btn-primary book-button" href="newspaper_booking.php?type=text">Book ad</a></br></br></br>
          </div>
          <div class="col-md-4" id="news_box">
		        <i><img src ="img/sample-classified-display-ad-big.jpg"  width="80%" /></i>
                <h4 class="service-heading text-center">Classified Display Ad</h4><hr>
                
                     <p class="text-muted">&#9670; <b>High visibility display classifieds</b>, more economical than display ads.</p>
                     <p class="text-muted">&#9670; Use <b>images/logos + formatting</b></p>
                     <p class="text-muted">&#9670; Rates <b>charged on a per sq.cm basis</b></p></br>
                 
     			<a class="btn btn-primary book-button" href="newspaper_booking.php?type=semi_display">Book ad</a></br></br></br>
          </div>
           <div class="col-md-4" id="news_box">
		        <i><img src ="img/sample-display-ad-big.jpg"  width="80%" /></i>
                <h4 class="service-heading text-center">Display Ad</h4><hr>
                
                     <p class="text-muted">&#9670; <b>HIGH IMPACT </b> advertisements and costly.</p>
                     <p class="text-muted">&#9670; <b>Customize ad</b> width, height & page choice with Full Page, Half Page, or other sizes.</p>
                     <p class="text-muted">&#9670; Higher <b>Visibility / Enhanced Response.</b></p></br>
                 
     			<a class="btn btn-primary book-button" href="newspaper_booking.php?type=display">Book ad</a></br></br></br>
          </div>
        </div>
      </div>
    </section>
    <?php
             include("footer.php");
     ?>
    </body>
</html>