<?php 
session_start();
if($_SESSION['login']){
	$msg ='<div class="alert-success" style="background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;color: #40e69a;float: left;font-size: 22px;padding: 7px;text-align: center;width: 83%;"><span>You have Successfully Logged In.</span></div>';
	$_SESSION['login'] = false;	
}
?>
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<?php echo $msg;?>
<div class="navbar-right">
    <ul class="nav navbar-nav">
       
        
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <span><?php echo $_SESSION['username'];?> <i class="caret"></i></span>
            </a>
            
            <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header bg-light-blue">
                    <img src="img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                        <?php echo $_SESSION['username'];?>
                    </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>
</nav>
