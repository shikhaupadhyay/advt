<?php include('includes/header.php');

if(!isset($_SESSION['id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
}
$id=$_GET['id'];
$result=mysql_query("SELECT n.id,n.name,e.id as edition_id,e.state_id,city_id FROM newspaper_ad as n left join newspaper_ad_edition as e on n.id=e.newspaper_id left join edition_city_map as m on n.id=m.newspaper_id  where n.id='".$id."'");

$edit_state_select=array();  
$edit_city_select=array();  
$edit_editon_select=array(); 
while($list=mysql_fetch_object($result)){
      $edit_newspaper_id=$list->id;
      $edit_newspaper=$list->name;
      array_push($edit_state_select,$list->state_id);
      array_push($edit_city_select,$list->city_id);
      array_push($edit_editon_select,$list->edition_id);
}   

?>

<script>
    $(function ()
        {
            $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
		    onStepChanging: function (event, currentIndex, priorIndex)
                {
                    var  move = true;
                    if(currentIndex=='0' && $('#newspaper_name').val()==''){
                        move = false; 			
				    }
                    if(currentIndex=='1' ){
                               	  if($('#edition_name').val()==''){
                                   move=false;
                                   }   
			       }
	                    
                     if(currentIndex=='2'){
                                   city=$('#city').val();
                                   if($('#city').val()==''){
                                   move=false;
                                   }  
                                     Selected_edition=[];
                                   $('#edition_name option:selected').each(function(){
                                        var edition = new Object();
                                      edition.id = $(this).val();
                                      edition.edition = $(this).text();
                                      Selected_edition.push(edition);	
                                   
                                       
                                   });
                                   
                                                                     
                                   $('#selected_ctiy').html('');
                                  $('#city option:selected').each(function(){
                                         data=$(this).text();
                                         value=$(this).val();
                                     var tr = document.createElement('tr');
                                        
                                      
                                       var td = document.createElement('td');									   
                                       td.innerHTML = "Select Edition for "+data;
                                        tr.appendChild(td); 
                                       var td2 = document.createElement('td');									   
                                       
                                        tr.appendChild(td2); 										

                                        var select = document.createElement("select");
                                        select.setAttribute("class", "form-control");
                                        select.setAttribute("name", data);
                                        select.setAttribute("id",value);
                                        td2.appendChild(select)
   
                                       var option = document.createElement("option");
                                        option.setAttribute("selected", "selected");
                                        option.setAttribute("disabled", "disabled");
                                        option.text = "select Edition";
                                        select.appendChild(option);

                                     for(var i = 0; i < Selected_edition.length; i++) {
                                        var obj = edition_json[i];
                                        var option = document.createElement("option");
                                         option.value = obj.id;
                                         option.text = obj.edition;
                                        select.appendChild(option);
                                       }
                                     document.getElementById("selected_ctiy").appendChild(tr);      
                                 })
                                   
                         return move 
                                  
			}
                    if(currentIndex=='0'){
                         var newspaper=$('#newspaper_name').val().toUpperCase();
                        
                         $.ajax({
                             url:'save_newspaper.php',
                             type: 'POST',
                             data:{newspaper:newspaper},
                             success: function(result){
                             
                                	var obj = JSON.parse(result);
				         	
                                        newspaper_id =obj.newspaper_id; 
								
					if(obj.msg =='exist'){
						move = false;
						alert("newpaper already exist")
										 
					}
                                         
                                 }
                                  
                         });
                    }
              return move;       
            },
            
	  onFinishing: function (event, currentIndex)
                       {     
                         //on click on finish button remove finish button and add 'Add' button in action url        
                                 
                                 
                           $(document).find(".actions ul").html('')  
                             var saveA = $("<a>").attr("href","#").attr("id","add").addClass("saveBtn").text("ADD");
                             var saveBtn = $("<li>").append(saveA); 
                             $(document).find(".actions ul").prepend(saveBtn)  
                             
                         //confirmation on sumbit    
                             var submit = confirm("Do you really want to submit the form");
                             
                       if (submit == true) {
                               city_edition_map=[];
                                
                                    
                                      
                          //create a json of city and edition maping 
                          
                                  for ( var i = 0, l = city.length; i < l; i++ ) {
                                        
                                      id=city[i];
			              city_edition_id=$("#"+id).val();
			              var city_edition = new Object();
                                      city_edition.city_id =id;
                                      city_edition.edition_id = city_edition_id;
                                      city_edition_map.push(city_edition);	
				     
                                      }
                                    var city_edition_json = JSON.stringify(city_edition_map);	
                                    					
                                           $.ajax({
                                                  url:'save_edition.php',
                                                  type: 'POST',
                                                  data:{city_edition_id_map:city_edition_json,state_id:state_id,newspaper_id:newspaper_id},
                                                 success: function(result){
                                                       window.location.href='newspaper.php?msg=upd';
                                                    }
                                  
                         });	
                                    
                                
                        }
                     return true;
                    }
        });
        
	$(".actions").on("click",".saveBtn",function(){
	            $('#wizard-t-1').click();
	            
        });
       
    });
    
 function fetch_edition(state){
                
     state_id=state;
      $.ajax({
          url: "fetch_state_edition.php",
         type: "POST",
         data: {state_id:state_id,newspaper_id:newspaper_id},
         success: function(data){
                   
              edition_json = $.parseJSON(data);
              $('#edition_name  option').remove(); 
              var $select = $('#edition_name');
	      var $option = $("<option/>").attr("value","").attr("selected","selected").attr("disabled","disabled").text("Select Edition");
              $select.append($option);
              
              for(var i = 0; i < edition_json.length; i++) {
                    var obj = edition_json[i];
                    var $option = $("<option/>").attr("value", obj.id).text(obj.edition);
                    $select.append($option);
              }
         }
    });          
    
  }
 function fetch_edit_edition(state,newspaper,edition){
                
     state_id=state;
     newspaper_id=newspaper;
     var edition_array=edition.split(",");
      $.ajax({
          url: "fetch_state_edition.php",
         type: "POST",
         data: {state_id:state_id,newspaper_id:newspaper_id},
         success: function(data){
                   
              edition_json = $.parseJSON(data);
              $('#edition_name  option').remove(); 
              var $select = $('#edition_name');
	      var $option = $("<option/>").attr("value","").attr("disabled","disabled").text("Select Edition");
              $select.append($option);
              
              for(var i = 0; i < edition_json.length; i++) {
                 var obj = edition_json[i];
                  if(edition_array.indexOf(obj.id) != -1)
                    {  
                        var $option = $("<option/>").attr("value", obj.id).attr("selected","true").text(obj.edition);
                     }
                    else{
                        var $option = $("<option/>").attr("value", obj.id).text(obj.edition);
                      }  
                    $select.append($option);
              }
         }
    });         
  } 
  
function save_edition(edition){
                

                edition_list = [];
                $("#ul li").each(function() { edition_list.push($(this).text()) }); 
                
               
                 $.ajax({
                 url: "add_edition.php",
                 type: "POST",
                 data: {state_id:state_id,edition:edition_list,newspaper_id:newspaper_id},
                 success: function(data){
                        
                        if(data=='success'){
                           $('#edition_model').modal('toggle');
                           fetch_edition(state_id);
                      }
                      else{
                         alert(data)
                     }
                 
                 
              }
      });
 }

function add_edition(){
          if(typeof state_id !== 'undefined'){
                $("#edition_list").html('');
                $('#edition_model').modal();
             
	 }
	 else{
		 alert("Select State First"); 
	 }
 }

function get_edition(){
        
           var text=$('#name').val().toUpperCase();
         
        var ul = document.getElementById("ul");  
        var li = document.createElement("li");  
        li.innerHTML = text+" ";
            
        li.onclick = function() {this.parentNode.removeChild(this);}
        
      
    if (ul.childElementCount == 0) {  
        ul.appendChild(li);      
    }
    else {
        ul.insertBefore(li, ul.firstChild);
    }

}

            </script>



<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Users

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Add Newpaper</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <!-- left column -->

            <div class="col-md-12">

                <!-- general form elements -->

                <div class="box box-primary">

                    <div class="box-header">
                          
                        <h3 class="box-title">Add Newpaper</h3>

                    </div><!-- /.box-header -->
                   <div id="message"></div>
                </div><!-- /.box -->

                        <div id="wizard">
                <h2>Add Newpaper</h2>
                <section>
                    
                        <div class="box-body">

                            <div class="form-group">

                                <label for="exampleInputfirstname">Newspaper</label>

                                <input type="text" class="form-control" placeholder="Enter Newspaper Name" style='width:65%;text-transform:uppercase' name="newspaper_name" id="newspaper_name" value="<?php echo $edit_newspaper;?>" data-validation="required" >

                            </div>
							<div class="form-group">

                                <label for="exampleInputlastname">Select Logo</label>
                                <input class="form-control" type="file" style='width:65%' name="fileToUpload" id="fileToUpload">	
					  

                            </div>                        
                           
                        </div><!-- /.box-body -->

                        
                </section>

                <h2>Select State </h2>
                <section>
                    <p><form role="form" action="add_newspaper_detail.php" method="post" enctype="multipart/form-data" name="state-form" id="state"> 
						  <div class="box-body">
                            <div class="form-group"  >
					<label for="exampleInputfirstname">Select State</label>
                                         <select required class="form-control" id="state" onchange="fetch_edition(this.value);" name="state">
					<option selected="true" disabled="disabled" value="">Select state </option>
					   <?php 
											 
						 $sql ="SELECT * FROM state";
						 $result=mysql_query($sql);
						while($list=mysql_fetch_array($result)){
						     if(in_array($list['0'], $edit_state_select)){
						     echo '<option value="'.$list['0'].'" name="a" selected>'.$list['1'].'</option>';
						            $edit_id=$list['0'];
						            $edited_editon_select=implode(",",$edit_editon_select);
						         echo "<script type='text/javascript'>fetch_edit_edition('$edit_id','$edit_newspaper_id','$edited_editon_select');</script>";
						     }
						     else{
						       echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
						      } 
						}
											
					  ?>
                           
                                         </select></br>
                                         
							 </div>
							</div> 
						
                        				
						  
						  <div class="box-body">
                            <div class="form-group">

                                <label for="exampleInputfirstname">Edition</label>
                                <select required class="form-control" id="edition_name" name="edition[]"  multiple>
					<option  disabled="disabled" >Select Edition </option>
					                
                                 </select></br>
                                <div class="box-footer">
			           <button type="button" class="btn btn-info "  id="addEdition" onclick="add_edition()">Add Edition</button>
	                        </div>

                          </div>
					               

                        </div><!-- /.box-body -->



                        <div class="box-footer">

                            
                        </div>

                </section>
                <h2>Select City</h2>
                <section>
                       
                        <div class="box-body">

                             <div class="form-group">

                                <label for="exampleInputfirstname">City</label>
                              <select required class="form-control" id="city" name="city[]" multiple >
				 <option  disabled="disabled" value="">Select City </option>
				    <?php 
				         $sql ="SELECT city_id,city_name FROM `city`";
					  $result=mysql_query($sql);
					   while($list=mysql_fetch_array($result)){
					        if(in_array($list['0'], $edit_city_select)){
						     echo '<option value="'.$list['0'].'" name="a" selected>'.$list['1'].'</option>';
						      
						     }
						     else{
						       echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
						      } 
					 	
					   }				
				   ?>
                           
                                         </select></br>

                            </div>                  
                           
                        </div><!-- /.box-body -->
                 </section>
                <h2>Final</h2>
                <section>
				        
                       
                        <table style="width:60%;" id="selected_ctiy"></table>
                        
                       
                        <!-- /.box-body -->

                        </form>
                
                </section>
               
            </div>

                

            </div><!--/.col (left) -->

            

        </div>   <!-- /.row -->

    </section><!-- /.content -->

</aside><!-- /.right-side -->

 <!-- edition Modal -->
  <div class="modal fade" id="edition_model" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
         
          <form method="post" action="">
             <div id="edition_list"></div>
           <div form-group>
               <input class="form-control"  name = "name" style='width:65%;text-transform:uppercase' id="name" type="text" placeholder="Name *" pattern="^[a-zA-Z][a-zA-Z-_\.]{1,20}$" required="required" data-validation-required-message="Please enter Edition Name.">
                  
           </div><br>
            <button type="button" class="btn btn-info " onclick="get_edition()" >Add</button>
<ul id="ul">
     </ul>
     <p>
         Click an item to remove it from the list.
    </p>  
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-info " onclick="save_edition()" >save</button> 
          <button type="button" class="btn btn-info " data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php include('includes/footer.php');?>


<link rel="stylesheet" href="jquery-steps/demo/css/normalize.css">
        <link rel="stylesheet" href="jquery-steps/demo/css/main.css">
        <link rel="stylesheet" href="jquery-steps/demo/css/jquery.steps.css">
        <script src="jquery-steps/lib/modernizr-2.6.2.min.js"></script>
        
        <script src="jquery-steps/lib/jquery.cookie-1.3.1.js"></script>
        <script src="jquery-steps/build/jquery.steps.js"></script>
        