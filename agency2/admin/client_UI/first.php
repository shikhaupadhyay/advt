<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8"> 
  <title>Digital Menu</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="style1.css">
  <link rel="stylesheet" href="header-fixed.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<title>Page Title</title>
</head>
<body>

<header class="header-fixed">

	<div class="header-limiter">

		<h1><a href="#">Digital Menu</a></h1>

		<nav>
			<a href="#">Home</a>
			
			<a href="new_Order.php">New Orders</a>
			<a href="pending_Orders.php">Pending Orders</a>
			<a href="cooking_Orders.php">In Process</a>
			<a href="delivered_Orders.php">Delivered Orders</a>
		</nav>

	</div>

</header>

<!-- You need this element to prevent the content of the page from jumping up -->
<div class="header-fixed-placeholder"></div>

  <div >
    <h1 class="banner-lead">
      <span class="banner-lead-1">the right ingredients</span>
      <span class="banner-lead-2">for the right food</span>
	  </br>
    </h1>
  
	<footer>
  <p>Posted by: Dhruv Tyagi</p>
  <p>Contact information: <a href="@example.com">someone@example.com</a>.</p>
</footer>
</body>
</html>