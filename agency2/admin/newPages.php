<?php include('includes/header.php');

if(!isset($_SESSION['id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
}

$imgurl =  $dbfn->siteUrl().'pagesimages/';
if($_POST['submit']){
		$data=array();	  
		 if($_FILES['file']['name'])
					 {
						$name = time().$_FILES["file"]["name"];
						$tmp_name = $_FILES["file"]["tmp_name"];
						move_uploaded_file($tmp_name, "pagesimages/$name");
					 }
			$data['table']['name']='pages';
			$data['data']['title']= $_POST['title'];
			$data['data']['content']= $_POST['content'];
			$data['data']['icon']=$imgurl.$name;
			$dbfn->insert($data);
			echo"<script>window.location.href='pageList.php?msg=add';</script>";
}

?>



<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Page

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li><a href="pageList.php">Pages</a></li>

            <li class="active">Add New Page</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <!-- left column -->

            <div class="col-md-12">

                <!-- general form elements -->

                <div class="box box-primary">

                    <div class="box-header">

                        <h3 class="box-title">Add new Page</h3>

                    </div><!-- /.box-header -->

                    <!-- form start -->

                    <form role="form" action="" method="post" enctype="multipart/form-data" name="user-form">

                    <?php echo $msg;?>

                        <div class="box-body">

                            <div class="form-group">

                                <label for="exampleInputTitle">Title</label>

                                <input type="text" class="form-control" placeholder="Enter title" style='width:65%' name="title" data-validation="required">

                            </div>

                             

                            <div class="form-group">

                                <label for="exampleInputContent">Content</label>

                                <textarea class="form-control" placeholder="Enter Content" style='width:65%' name="content"></textarea>

                            </div>
                            
                            <div class="form-group">

                                <label for="exampleInputIcon">Icon</label>

                                <input type="file" class="form-control" name="file"  style='width:65%; height: auto;' >

                            </div>

                            

                           

                        </div><!-- /.box-body -->



                        <div class="box-footer">

                            <input type="submit" class="btn btn-primary" value="Submit" name="submit">

                        </div>

                    </form>

                </div><!-- /.box -->



                

            </div><!--/.col (left) -->

            

        </div>   <!-- /.row -->

    </section><!-- /.content -->

</aside><!-- /.right-side -->

<?php include('includes/footer.php');?>

            