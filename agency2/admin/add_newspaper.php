<?php include('includes/header.php');

if(!isset($_SESSION['id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
}
      
?>

<script>
    $(function ()
        {
            $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
		    onStepChanging: function (event, currentIndex, priorIndex)
                {
                    var  move = true;
                    if(currentIndex=='0' && $('#newspaper_name').val()==''){
                        move = false; 			
				    }
                    if(currentIndex=='1' && $('#state').val()=='' && $('#edition').val()=='' && $('#city').val()==''){
                        move = false; 			
			        }
                     if(currentIndex=='2'){
                       for (var i = 0; i < undefine_city.length; i++) {
                               
                               var select = document.createElement("select");
                               select.setAttribute("name", undefine_city[i]);
                               select.setAttribute("id",undefine_city[i]);
                                                                          
                              document.getElementById("selected_ctiy").appendChild(select);

                             
                             for(var i = 0; i < edition_json.length; i++) {
                                      var obj = edition_json[i];
                                   var option = document.createElement("option");
                                option.value = obj.edition;
                                 option.text = obj.edition;
                            select.appendChild(option);
                          }
                      }		
			        }
                    if(currentIndex=='0'){
                        var newspaper=$('#newspaper_name').val();
                            $.ajax({
                                url:'save_newspaper.php',
                                type: 'POST',
                                data:{newspaper:newspaper},
                                success: function(result){
									var json = $.parseJSON(result);
									var obj = json[0];
									newspaper_id =obj.newspaper_id; 
									
									if(obj.msg =='exist'){
										move = false;
										alert("newpaper already exist")
												 
									}
                                         
                                }
                                  
                            });
                    }
                    return move;    
            },
				onFinishing: function (event, currentIndex)
                    {
                        alert("Submitted!");
                    }
        });
    });
    function fetch_edition(state){
                
                 state_id=state;
                
                $.ajax({
              url: "fetch_state_city.php",
              type: "POST",
              data: {state_id:state_id},
              success: function(data){
                   
                    edition_json = $.parseJSON(data);
                   $('#edition_name  option').remove(); 
                   var $select = $('#edition_name');
				    var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select Edition");
                                  $select.append($option);
                 for(var i = 0; i < edition_json.length; i++) {
                  var obj = edition_json[i];
                   var $option = $("<option/>").attr("value", obj.edition).text(obj.edition);
                                  $select.append($option);
              
                }
            }
      });
  }
function save_edition(edition){
                $('#edition_modal').modal('hide');

               edition = $("#name").val();
                 $.ajax({
                 url: "save_edition.php",
                 type: "POST",
                 data: {state_id:state_id,edition:edition},
                 success: function(data){
                       if(data=='success'){
                           
                           fetch_edition(state_id);
                       }
                       else{
                         alert(data)
                       } 
                       
                 
              }
      });
 }

function compare_city(){
                      
                       
                     city=$('#city').val();
                     edition =$('#edition_name').val();
                     for (var i = 0; i < city.length; i++) {
                           find_city=[];
                           undefine_city=[];
                         if(edition.indexOf(city[i])>=0){
                             
                             find_city.push(city[i]);
                          }
                         else{
                            
                            undefine_city.push(city[i]);
                         }
                         alert(undefine_city);
                       
                        }
                     $.ajax({
                            url: "compare_city_edition.php",
                            type: "POST",
                            data: {city:select_city},
                            success: function(data){
                            
                             var json = $.parseJSON(data);
                   $('#selected_edition_name  option').remove(); 
                   var $select = $('#selected_edition_name');
				    var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Selected Edition");
                                  $select.append($option);
                 for(var i = 0; i < edition_json.length; i++) {
                           var obj1 = edition_json[i];
                           var obj2 = json[i];
			        
                              
                   if(obj1.edition==obj2.edition){
                   var $option = $("<option/>").attr("value", obj1.id).attr('selected','selected').text(obj1.edition);
                                  
                   }
                   else{
                      var $option = $("<option/>").attr("value", obj1.id).text(obj1.edition);
                                 
                   } 
                      $select.append($option);
                }
            }
      });
                      
                  
                            							 
		      }

            </script>



<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Users

        </h1>

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Add Newpaper</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <!-- left column -->

            <div class="col-md-12">

                <!-- general form elements -->

                <div class="box box-primary">

                    <div class="box-header">
                          
                        <h3 class="box-title">Add Newpaper</h3>

                    </div><!-- /.box-header -->
                   <div id="message"></div>
                </div><!-- /.box -->

                        <div id="wizard">
                <h2>Add Newpaper</h2>
                <section>
                    
                        <div class="box-body">

                            <div class="form-group">

                                <label for="exampleInputfirstname">Newspaper</label>

                                <input type="text" class="form-control" placeholder="Enter Newspaper Name" style='width:65%' name="newspaper_name" id="newspaper_name" data-validation="required" >

                            </div>
							<div class="form-group">

                                <label for="exampleInputlastname">Select Logo</label>
                                <input class="form-control" type="file" style='width:65%' name="fileToUpload" id="fileToUpload">	
					  

                            </div>                        
                           
                        </div><!-- /.box-body -->

                        </form>
                </section>

                <h2>Select State </h2>
                <section>
                    <p><form role="form" action="add_newspaper_detail.php" method="post" enctype="multipart/form-data" name="state-form" id="state"> 
						  <div class="box-body">
                            <div class="form-group"  >
										    <label for="exampleInputfirstname">Select State</label>
                                         <select required class="form-control" id="state" onchange="fetch_edition(this.value);" name="state">
												<option selected="true" disabled="disabled" value="">Select state </option>
												<?php 
											 
													 $sql ="SELECT * FROM state";
													 $result=mysql_query($sql);
													 while($list=mysql_fetch_array($result)){
														echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
													 }
											
												?>
                           
                                         </select></br>
                                         
							 </div>
							</div> 
						
                        				
						  
						  <div class="box-body">
                            <div class="form-group">

                                <label for="exampleInputfirstname">Edition</label>
                                <select required class="form-control" id="edition_name" name="edition[]"  multiple>
					<option selected="true" disabled="disabled" >Select Edition </option>
					                
                                 </select></br>
                                <div class="box-footer">
			           <button type="button" class="btn btn-info " data-toggle="modal" data-target="#edition_model">Add Edition</button>
	                        </div>

                          </div>
					               

                        </div><!-- /.box-body -->



                        <div class="box-footer">

                            
                        </div>

                </section>
                <h2>Select City</h2>
                <section>
                        <p id="label"></p>
                        <div id="selected_city">
                            
                       </div>
                        <div class="box-body">

                             <div class="form-group">

                                <label for="exampleInputfirstname">City</label>
                              <select required class="form-control" id="city" name="city[]" multiple onchange="compare_city()">
												<option selected="true" disabled="disabled" value="">Select City </option>
												<?php 
											 
													 $sql ="SELECT city_id,city_name FROM `city`";
													 $result=mysql_query($sql);
													 while($list=mysql_fetch_array($result)){
														echo '<option value="'.$list['1'].'" name="a">'.$list['1'].'</option>';
													 }
											
												?>
                           
                                         </select></br>

                            </div>                  
                           
                        </div><!-- /.box-body -->
                 </section>
                <h2>Fourth Step</h2>
                <section>
                        <div id="selected_ctiy"></div>
                        
                        </div><!-- /.box-body -->

                        </form>
                
                </section>
               
            </div>

                

            </div><!--/.col (left) -->

            

        </div>   <!-- /.row -->

    </section><!-- /.content -->

</aside><!-- /.right-side -->

 <!-- edition Modal -->
  <div class="modal fade" id="edition_model" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
         <p>Please select state first </p>
          <form method="post" action="">
           <div form-group>
               <input class="form-control"  name = "name" id="name" type="text" placeholder="Name *" pattern="^[a-zA-Z][a-zA-Z-_\.]{1,20}$" required="required" data-validation-required-message="Please enter Edition Name.">
                  
           </div><br>

          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-info " onclick="save_edition()" data-dismiss="modal">save</button> 
          <button type="button" class="btn btn-info " data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<?php include('includes/footer.php');?>

<script type="text/javascript" src="js/newspaper.js"></script>
<link rel="stylesheet" href="jquery-steps/demo/css/normalize.css">
        <link rel="stylesheet" href="jquery-steps/demo/css/main.css">
        <link rel="stylesheet" href="jquery-steps/demo/css/jquery.steps.css">
        <script src="jquery-steps/lib/modernizr-2.6.2.min.js"></script>
        
        <script src="jquery-steps/lib/jquery.cookie-1.3.1.js"></script>
        <script src="jquery-steps/build/jquery.steps.js"></script>