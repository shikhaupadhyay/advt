<!DOCTYPE html>
<html lang="en">

  <head>
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tripura Advt Agency</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
	<link href="css/header.css" rel="stylesheet">
   
  </head>

  <body id="page-top">
  <div class="container">
  <?php 
      include("includes/connection.php");
	   include("header.php");
       
      ?>
   </div>
         <!-- select tab -->
	<section id="services"> 
        <div class="container">
		    <div class="row text-center">
                <div class="col-lg-12 ">
                    <div id=news_box >
					    <h4 class=" text-muted ">You have selected </h4>
		                    <?php
								$type=$_GET["type"];
							    $query=mysql_query("SELECT name,type_image FROM ad_type where type='".$type."'");
							    $list= mysql_fetch_object($query);
								 echo $list->name;
                            ?>								 
							     
						<p><img  src ="<?php echo $list->type_image ; ?>"  width="120px" height="120px"></p>
										
						<p><a class="btn btn-primary " href="book_ad_step1.php">Change</a></p></br>
						    
					</div>
                </div>
            </div></br>
			
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">Book Ad.</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
			<div>
				<div class="row ">
					<div class="col-lg-12 ">
						<div id="error">
							
						</div>
					</div>
				</div></br>
			</div>
			<ul class="nav nav-tabs">
                 <li class="active" ><a class="nav-link" data-toggle="tab" href="#home">Select Your Newspaer</a></li>
                 <li><a class="nav-link" data-toggle="tab" href="#menu1">Target By Specific Location</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" class="active tab-pane in "></br></br>
   				    <div class="row">
                        <div class="col-lg-12">
                            <form id="contactForm" name="sentMessage" novalidate="novalidate">
                            <div class="row">
	                            <div class="col-md-12">
					        		<fieldset class="border p-2 form-group">
                                         <legend class="w-auto">Ad category</legend>
										 <div class="form-group"  >
                                         <select class="form-control" id="category" onchange="fetch_subcategory()" name="category">
												<option selected="true" disabled="disabled">Select Ad category </option>
												<?php 
											 
													 $sql ="SELECT * FROM ad_category";
													 $result=mysql_query($sql);
													 while($list=mysql_fetch_array($result)){
														echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
													 }
											
												?>
                           
                                         </select>
                                         <p class="help-block text-danger"></p>
							            </div>
										<div class="form-group">
											<select required="required" class="form-control"  id="subcategory" >
												<option selected="true" disabled="disabled">Select Ad Subcategory </option>
																  
											</select>
											<p class="help-block text-danger"></p>
										</div> 
									</fieldset>	
									<fieldset class="border p-2 form-group">
										<legend class="w-auto">Newspaer</legend> 
										<div class="form-group"  >
											<select required="required" class="form-control" id="newspaper" onchange="myFunction()" required >
													<option selected="true" disabled="disabled">Select Newspaer</option>
													<?php 
														$sql ="SELECT * FROM newspaper_ad";
														$result=mysql_query($sql);
														while($list=mysql_fetch_array($result)){
															 echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
														}
											
													?>                           
											</select>
											<p class="help-block text-danger"></p>
										</div>
											
										<div class="form-group" >
											<select required="required" id="edition" class="form-control"  >
													<option selected="true" disabled="disabled">Select Edition</option>
											</select>
											<p class="help-block text-danger"></p>
										</div>
									</fieldset>	
									<fieldset class="border p-2 form-group">
										<legend class="w-auto">Personal Details</legend> 
										<div class="form-group"  >
										    <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">	
											<p class="help-block text-danger"></p>
										</div>
										<div class="form-group"  >
											 <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
											 <p class="help-block text-danger"></p>
										</div>
										<div class="form-group" >
										     <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
											 <p class="help-block text-danger"></p>
										</div>
									</fieldset>	
									<div class="form-group">
									    <textarea class="form-control" id="message" placeholder="Ad Content*" required="required" data-validation-required-message="Please enter Your content."></textarea>
									    <p class="help-block text-danger"></p>
									</div>
					 
                                </div>
                            </div>
                        <div class="clearfix"></div>
						<div class="col-lg-12 text-center">
						  <div id="success"></div>
						  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit" onclick="return validateForm()">Book Ad</button>
						</div>
                    </div>
            </form>
          </div>
        </div>
        
              <div id="menu1" class="tab-pane fade">
       
              </div>
             <div id="menu1" class="tab-pane fade">
     
              </div>
              <div id="menu3" class="tab-pane fade">
      
              </div>
         </div>
      </div>
    
        
    </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Your Website 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <script src="js/booking.js"></script>

    <!-- Custom scripts for this template -->
   
	
  </body>

</html>
