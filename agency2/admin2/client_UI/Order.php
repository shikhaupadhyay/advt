<?php
   class Order {
      /* Member variables */
      var $order_id;
      var $uid;
	  var $created_date;
	  var $status;
	  var $items = array();	  
		 
	  function __construct($order_id, $uid, $created_date,$status)
	  {
		  $this->order_id = $order_id;
		  $this->uid = $uid;
		  $this->created_date = $created_date;
		  $this->status=$status;
	  }	  
	 function AddItem($item)	  
	 {
		 
		  $this->items[] = $item;
		
	 }
	
	}

	class Item {
		//var $productId;
		var $title;
		var $quantity;
		var $personalization;
		
		function __construct($personalization, $title, $quantity)
		{
			$this->personalization = $personalization;
			$this->title = $title;
			$this->quantity = $quantity;
			
		}
	}
?>	  