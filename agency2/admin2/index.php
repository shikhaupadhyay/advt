<?php 

include('includes/connection.php');

include('classes/AuthLogin.php');

if(isset($_SESSION['id'])){

	echo "<script>window.location.href='cvdashBoard.php'</script>";

	}

if($_SESSION['logout'])

{

	$msg ='<div class="alert-success" style="padding: 7px;"><span>You have Successfully Logout.</span></div>';

}

if(isset($_REQUEST['login'])){

		$login = new AuthLogin();

		$msg = $login->adminlogin();

	}

?>

<!DOCTYPE html>

<html class="bg-black">

    <head>

        <meta charset="UTF-8">

        <title>Admin | Login</title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- bootstrap 3.0.2 -->

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- font Awesome -->

        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->

        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />



    </head>

    <body class="bg-black">



        <div class="form-box" id="login-box">

            <div class="header">Admin <span style="color:#ff3f3f">Panel</span></div>

            <form action="" method="post">

                <div class="body bg-gray">

                <h3 class="form-title">Login to your account</h3>

                <?php echo $msg;?>

                    <div class="form-group">

                    <i class="fa fa-user"></i>

                        <input type="text" name="username" class="form-control" placeholder="Username" value="<?php if(isset($_COOKIE['user'])){echo $_COOKIE['user'];} ?>"/>

                    </div>

                    <div class="form-group">

                    <i class="fa fa-lock"></i>

                        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php if(isset($_COOKIE['pass'])){echo $_COOKIE['pass'];} ?>"/>

                    </div>          

                    <div class="form-group">

                        <input type="checkbox" name="remember_me" <?php if(isset($_COOKIE['user'])) {echo "checked=checked"; } ?>/> <span style="color:#fff; font-size: 16px;">Remember me</span>

                        <button type="submit" class="btn blue pull-right" name="login">Login<i class="fa fa-arrow-right"></i></button>

                    </div>

                </div>

                

            </form>



           </div>



    </body>

</html>