<?php include('includes/header.php');

if(!isset($_SESSION['id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
}

$id = $_REQUEST['id'];
if($id){
	$query = $dbfn->deleteRec('pages',$id);
	if($query){
		$msg = "<div class='error'>1 Record Delete Successfully.</div>";
		}
}

$selectpages = "select * from pages";
$querypages = mysql_query($selectpages) or die(mysql_error());
if($_REQUEST['msg']=='add'){
	$msg = "<div class='success'>1 New Page Add Successfully.</div>";
	}
if($_REQUEST['msg']=='upd'){
	$msg = "<div class='success'>Page Updated Successfully.</div>";
	}
?>

<!-- Right side column. Contains the navbar and content of the page -->

            <aside class="right-side">

                <!-- Content Header (Page header) -->

                <section class="content-header">

                    <h1>

                        Pages List

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li><a href="#">Pages</a></li>

                        <li class="active">Pages Lists</li>

                    </ol>

                </section>

			<div class="add-new"><a href="newPages.php">Add New</a></div>

                <!-- Main content -->

                <section class="content">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="box">

                                <div class="box-header">

                                    <h3 class="box-title">Pages List</h3>

                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive">
								
                                	<?php echo $msg;?>
                                    <table id="example2" class="table table-bordered table-hover">

                                        <thead>

                                            <tr>

                                                <th>Id</th>

                                                <th>Title</th>

                                                <th>Icon</th>
                                                
                                                 <th>Action</th>

                                              

                                            </tr>

                                        </thead>

                                        <tbody>

                                        <?php while($row=mysql_fetch_object($querypages)){?>

                                            <tr>

                                                <td><?php echo $row->id;?></td>

                                                <td><?php echo $row->title;?></td>
                                                
                                                 <td><img src="<?php echo $row->icon;?>" width="50" height="50"></td>

                                                <td><a href="editpage.php?id=<?php echo $row->id;?>">Edit</a> | <a href="pageList.php?id=<?php echo $row->id;?>" onClick="return confirm('Are you sure to delete Page');">Delete </a></td>

                                               

                                            </tr>

                                     <?php }?>  

                                        </tbody>

                                        

                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->



                            

                        </div>

                    </div>



                </section><!-- /.content -->

            </aside><!-- /.right-side -->

            <?php include('includes/footer.php');?>

            