<section class="sidebar">

<!-- Sidebar user panel -->

<div class="user-panel">

    <div class="pull-left image">

        <img src="img/avatar.png" class="img-circle" alt="User Image" />

    </div>

    <div class="pull-left info">

        <p>Hello, <?php echo $_SESSION['username'];?></p>

    </div>

</div>



<!-- sidebar menu: : style can be found in sidebar.less -->

<ul class="sidebar-menu">

    <li class="active">

        <a href="cvdashBoard.php">

            <i class="fa fa-dashboard"></i> <span>Dashboard</span>

        </a>

    </li>

    

  <li class="treeview">

        <a href="#">

            <i class="fa fa-laptop"></i>

            <span>Users</span>

            <i class="fa fa-angle-left pull-right"></i>

        </a>

        <ul class="treeview-menu">

            <li><a href="userList.php"><i class="fa fa-angle-double-right"></i> User List</a></li>

            <li><a href="user.php"><i class="fa fa-angle-double-right"></i> Add New User</a></li>

        </ul>

    </li>

  
    

    <li class="treeview">

        <a href="#">

            <i class="fa fa-th"></i>

            <span>Booking</span>

            <i class="fa fa-angle-left pull-right"></i>

        </a>

        <ul class="treeview-menu">

            <li><a href="booking.php"><i class="fa fa-angle-double-right"></i> All Booking</a></li>

            <li><a href="newbooking.php"><i class="fa fa-angle-double-right"></i> Add New booking</a></li>

            

        </ul>

    </li>
    <li class="treeview">

        <a href="#">

            <i class="fa fa-laptop"></i>

            <span>Online</span>

            <i class="fa fa-angle-left pull-right"></i>

        </a>

        <ul class="treeview-menu">

            <li><a href="channel.php"><i class="fa fa-angle-double-right"></i> All Channel</a></li>

            <li><a href="newchannel.php"><i class="fa fa-angle-double-right"></i> Add New Channel</a></li>

            

        </ul>

    </li>

	 

   
    </ul>

</section>

<!-- /.sidebar -->
