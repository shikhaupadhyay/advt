<?php include('includes/header.php');

if(!isset($_SESSION['id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
}

$id = $_REQUEST['id'];
if($id){
	$query = $dbfn->deleteRec('products',$id);
	if($query){
		$msg = "<div class='error'>1 Product Delete Successfully.</div>";
		}
}

$selectproducts = "select * from products";
$queryproducts = mysql_query($selectproducts) or die(mysql_error());

if($_REQUEST['msg']=='add'){
	$msg = "<div class='success'>1 New Product Add Successfully.</div>";
	}
if($_REQUEST['msg']=='upd'){
	$msg = "<div class='success'>Product Updated Successfully.</div>";
	}
?>

<!-- Right side column. Contains the navbar and content of the page -->

            <aside class="right-side">

                <!-- Content Header (Page header) -->

                <section class="content-header">

                    <h1>

                        Product List

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li><a href="#">Category</a></li>

                        <li class="active">Product Lists</li>

                    </ol>

                </section>

			<div class="add-new"><a href="newProduct.php">Add New</a></div>

                <!-- Main content -->

                <section class="content">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="box">

                                <div class="box-header">

                                    <h3 class="box-title">Product List</h3>

                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive">
								
                                	<?php echo $msg;?>
                                    <table id="example2" class="table table-bordered table-hover">

                                        <thead>

                                            <tr>

                                                <th>Id</th>
                                                <th>CatId</th>
                                                <th>Product Name</th>
                                                <th>Image</th>
                                                <th>Price</th>
                                                <th>Action</th>                                             

                                            </tr>

                                        </thead>

                                        <tbody>

                                        <?php while($row=mysql_fetch_object($queryproducts)){?>

                                            <tr>

                                                <td><?php echo $row->id;?></td>
												<td><?php echo $row->cid;?></td>
                                                <td><?php echo $row->title;?></td>
                                                
                                                 <td><img src="<?php echo $row->image;?>" width="50" height="50"></td>
												<td><?php echo $row->price;?></td>
                                                <td><a href="editproduct.php?id=<?php echo $row->id;?>">Edit</a> | <a href="products.php?id=<?php echo $row->id;?>" onClick="return confirm('Are you sure to delete product');">Delete </a></td>

                                               

                                            </tr>

                                     <?php }?>  

                                        </tbody>

                                        

                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->



                            

                        </div>

                    </div>



                </section><!-- /.content -->

            </aside><!-- /.right-side -->

            <?php include('includes/footer.php');?>

            