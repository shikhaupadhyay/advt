	
 function myFunction() {
         var id = document.getElementById("newspaper").value;
         $.ajax({
              url: "fetch_edition.php",
              type: "POST",
              
              data: {id:id},
              success: function(data){
                   
                   var json = $.parseJSON(data);
                   $('#edition  option').remove(); 
                   var $select = $('#edition');
				    var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select Edition");
                                  $select.append($option);
                 for(var i = 0; i < json.length; i++) {
                  var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.id).text(obj.edition);
                                  $select.append($option);
              
                }
            }
      });
      }
	  function fetch_subcategory() {
         var id = document.getElementById("category").value;
         $.ajax({
              url: "fetch_subcategory.php",
              type: "POST",
              
              data: {id:id},
              success: function(data){
                   
                   var json = $.parseJSON(data);
                   $('#subcategory  option').remove(); 
                   var $select = $('#subcategory');
				     var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select subcategory");
                                  $select.append($option);
                   for(var i = 0; i < json.length; i++) {
                     var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.id).text(obj.name);
                                  $select.append($option);
              
                }
            }
      });
      }  
	  function fetch_location_subcategory(){
         var id = document.getElementById("location_category").value;
         $.ajax({
              url: "fetch_subcategory.php",
              type: "POST",
              
              data: {id:id},
              success: function(data){
                 var json = $.parseJSON(data);
                   $('#location_subcategory  option').remove(); 
                   var $select = $('#location_subcategory');
				     var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select subcategory");
                                  $select.append($option);
                   for(var i = 0; i < json.length; i++) {
                     var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.id).text(obj.name);
                                  $select.append($option);
              
                }
            }
      });
      }  
	  function fetchcity() {
         var edition_id = document.getElementById("edition").value;
		 var newspaper_id = document.getElementById("newspaper").value;
		 
         $.ajax({
              url: "fetch_edition_city.php",
              type: "POST",
              
              data: {edition_id:edition_id,newspaper_id:newspaper_id},
              success: function(data){
                   
                   var json = $.parseJSON(data);
                   $('#edition_city  option').remove(); 
                   var $select = $('#edition_city');
				     var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select Edition");
                                  $select.append($option);
                   for(var i = 0; i < json.length; i++) {
                     var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.city).text(obj.city);
                                  $select.append($option);
              
                }
            }
      });
      } 
	  function fetchnewspaer(){
         var state_id = document.getElementById("state").value;
		 var city_id = document.getElementById("city").value;
		
         $.ajax({
              url: "fetch_newspaper.php",
              type: "POST",
              
              data: {state_id:state_id,city_id:city_id},
              success: function(data){
                   
                   var json = $.parseJSON(data);
                   $('#city_newspaper  option').remove(); 
                   var $select = $('#city_newspaper');
				     var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select Newspaper");
                                  $select.append($option);
                   for(var i = 0; i < json.length; i++) {
                     var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.newspaper_id).text(obj.name);
                                  $select.append($option);
              
                }
            }
      });
      } 
      function fetch_city() {
            var state_id = document.getElementById("state").value;
         $.ajax({
              url: "fetch_city.php",
              type: "POST",
              
              data: {state_id:state_id},
              success: function(data){
                   
                   var json = $.parseJSON(data);
                   $('#city  option').remove(); 
                   var $select = $('#city');
				    var $option = $("<option/>").attr("value","").attr("selected","true").attr("disabled","disabled").text("Select City");
                                  $select.append($option);
                 for(var i = 0; i < json.length; i++) {
                  var obj = json[i];
                   var $option = $("<option/>").attr("value", obj.city).text(obj.city);
                                  $select.append($option);
              
                }
            }
      });
      }	  
