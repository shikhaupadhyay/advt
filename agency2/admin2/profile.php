<?php include('includes/header.php');
$condition = 'where id='.$_SESSION['id'];
if($_REQUEST['update']){
$data['table']['name']='admin_member';
$data['data']['name']=$_REQUEST['name'];
$data['data']['username']=$_REQUEST['username'];
$data['data']['email']=$_REQUEST['email'];
$data['data']['contact']=$_REQUEST['contact'];
$data['where']['id']=$_SESSION['id'];
$updaterow = $dbfn->updatequery($data);
}

$row = $dbfn->getdataByid('admin_member',$condition);
?>
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        User Account <small>Profile</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">User Account </li>
                    </ol>
                </section>
                <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Profile Account</h3>
                        <?php echo $message;?>
                    </div><!-- /.box-header -->
                    <div class="tabbing">
                            <ul class="tab">
                                <li><a href="javascript:void(0);">Profile Info</a></li>
                                <li><a href="javascript:void(0);">Account</a></li>
                                <?php /*?><li><a href="javascript:void(0);">Change Password</a></li>
                                <li><a href="javascript:void(0);">Change Avatar</a></li><?php */?>
                            </ul>
                            <div class="open1">
                                
                                    <div class="box-body">
                                         <div class="col-md-2 profile-img">
                                        	<img src="http://localhost/php/project/admin/assets/img/profile/Koala.jpg" alt="">	
                                         </div>
                                          <ul class="col-md-10 profile-info">
                                              <?php if($row->name){ ?><li><span>Name: </span><?php echo $row->name;?></li><?php } ?>
                                              <?php if($row->username){ ?><li><span>Username: </span> <?php echo $row->username;?></li><?php } ?>
                                              <?php if($row->email){ ?><li><span>Email: </span><a href="mailto:<?php echo $row->email;?>"> <?php echo $row->email;?></a></li><?php } ?>
                                              <?php if($row->contact){ ?><li><span>Contact: </span><a href="tel:<?php echo $row->contact;?>"><?php echo $row->contact;?></a></li><?php } ?>
                                          </ul>                                       
                                    </div>
                            </div>
                            <div class="open1">
                                <form role="form" method="post" name="myform" action="" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" style="width:65%" value="<?php echo $row->name;?>">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Username</label>
                                            <input type="text" name="username" class="form-control" placeholder="Enter Username" style="width:65%" value="<?php echo $row->username;?>">
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Enter email" style="width:65%" value="<?php echo $row->email;?>">
                                        </div>
                                        
                                                                  
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact no.</label>
                                            <input type="contact" name="contact" class="form-control" placeholder="Enter Contact No." style="width:65%" value="<?php echo $row->contact;?>">
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <input type="submit" class="btn btn-primary" name="update" value="Update Info">
                                    </div>
                   				 </form>
                            </div>
                            <div class="open1">
                                <form role="form" method="post" name="myform" action="" enctype="multipart/form-data">
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Current Password</label>
                                            <input type="password" name="password" class="form-control" placeholder="Current Password" style="width:65%">
                                        </div>
                                         
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">New Password</label>
                                            <input type="password"  name="npassword" class="form-control" placeholder="New Password" style="width:65%">
                                        </div>
                                         
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">ReEnter New Password</label>
                                            <input type="password" name="rpassword" class="form-control" placeholder="ReEnter New Password" style="width:65%">
                                        </div>
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <input type="submit" name="changepass" class="btn btn-primary" value="Change Password">
                                    </div>
                    			</form>
                            </div>
                            <div class="open1">
                                <form role="form" method="post" name="myform" action="" enctype="multipart/form-data">
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputFile">File input</label>
                                            <input type="file" id="exampleInputFile">
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Change Avatar</button>
                                    </div>
                    			</form>
                            </div>
						</div>
                    <!-- form start -->
                    
                </div><!-- /.box -->

                
            </div><!--/.col (left) -->
            
        </div>   <!-- /.row -->
    </section>
	
</aside><!-- /.right-side -->
<?php include('includes/footer.php');?>
<script>
 $(window).load(function(){
    $('.tabbing').children('.open1').hide();
    $('.tabbing').children('.open1').eq(0).show();
    $('ul.tab li').children('a').eq(0).addClass('active');
});
    $(document).ready(function(){
        $('ul.tab li').click(function(){
            $('ul.tab li').children('a').removeClass('active');
            $('.tabbing').children('.open1').hide();
            $(this).children('a').addClass('active');
            $('.tabbing').children('.open1').eq($(this).index()).show();
        });
    });
</script>