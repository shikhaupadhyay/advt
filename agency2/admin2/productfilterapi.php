<?php
	include('./includes/connection.php');
	ini_set('display_errors','0');
	
	if(isset($_REQUEST['filter']) && !empty($_REQUEST['filter'])){
		$Filter	= 	json_decode($_REQUEST['filter']);
		$Cid	=	json_decode($Filter->cid);
		
		$Veg		=	$Cid[0];
		$NonVeg		=	$Cid[1];
		$Spicy		=	$Cid[2];
		$NonSpicy	=	$Cid[3];
		$Price		=	$Cid[4];
		$Keywords	=	$Cid[5];
		
		if(isset($Veg) && !empty($Veg)){
			$ProductType	=	'0';
		}else{
			$ProductType	=	'';
		}
		
		if(isset($NonVeg) && !empty($NonVeg)){
			$ProductType1	=	'1';
		}else{
			$ProductType1	=	'';
		}
		
		if(isset($Spicy) && !empty($Spicy)){
			$SpicyType	=	'0';
		}else{
			$SpicyType	=	'';
		}
		
		if(isset($NonSpicy) && !empty($NonSpicy)){
			$SpicyType1	=	'1';
		}else{
			$SpicyType1	=	'';
		}
		
		
		$Ext   = '';	
		$Query = "Select t1.id, t1.title , t1.image , t1.description, t1.price from products as t1 inner join product_type as t2 on t1.id = t2.ProductId ";
		if((isset($ProductType) && $ProductType != '') || (isset($ProductType1) && $ProductType1 != '')){
			if(isset($Ext) && !empty($Ext)){
				$Ext .= " and t2.ProductsType = '".$ProductType."' or t2.ProductsType = '".$ProductType1."'";
			}else{
				$Ext = " where t2.ProductsType = '".$ProductType."' or t2.ProductsType = '".$ProductType1."'";
			}
		}
		
		if((isset($SpicyType) && $SpicyType != '') || (isset($SpicyType1) && $SpicyType1 != '')){
			if(isset($Ext) && !empty($Ext)){
				$Ext .= " and t2.SpicyType = '".$SpicyType."' or t2.SpicyType = '".$SpicyType1."'";
			}else{
				$Ext = " where t2.SpicyType = '".$SpicyType."' or t2.SpicyType = '".$SpicyType1."'";
			}
		}
		
		if(isset($Price) && $Price != ''){
			if(isset($Ext) && !empty($Ext)){
				$Ext .= " and t1.price <= '".$Price."' ";
			}else{
				$Ext = " where t1.price <= '".$Price."' ";
			}
		}
		
		if(isset($Keywords) && $Keywords != ''){
			if(isset($Ext) && !empty($Ext)){
				$Ext .= " and t1.description like '%".$Keywords."%'";
			}else{
				$Ext = " where t1.description like '%".$Keywords."%'";
			}
		}
		
		if(isset($Ext) && !empty($Ext)){
			$Query .= $Ext;
		}
		
		$Query .= "  order by t1.title ASC ";
		
		$ResultArray  =  $dbfn->Query($Query , 1);
		
		if(count($ResultArray)){
			
			$Products = array();
			$i  = 0;
			foreach($ResultArray as $key=>$value){
				$Products[$i]['id'] 			= 	$value['id'];
				$Products[$i]['title'] 			= 	$value['title'];
				$Products[$i]['image'] 			= 	$value['image'];
				$Products[$i]['price'] 			= 	number_format($value['price']);
				$Products[$i]['description'] 	= 	$value['description'];
				$i++;
			}
			
			$Response['products'] 		= $Products;
			$Response['apimessage'] 	= 'Success';
			echo json_encode($Response);
			exit;
			
		}else{
			$Response['apimessage'] = 'No Records Found';
			echo json_encode($Response);
			exit;
		}
		
		
	}else{
		$Response['apimessage'] = 'Parameter Missing';
		echo json_encode($Response);
		exit;
	}
	

?>