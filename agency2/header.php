<!DOCTYPE html>
<html lang="en">

  <head>
     
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tripura Advt Agency</title>
  
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
         <link href="css/agency.css" rel="stylesheet">
	<link href="css/header.css" rel="stylesheet">

  </head>

  <body id="page-top">
     
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php">Tripura Advt Agency</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
			    <div class="dropdown">
				    <a class=" nav-link js-scroll-trigger dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" data-hover="dropdown">Book Ad.
                    <span class="caret"></span></a>
                       <ul class="dropdown-menu">
                         <li><a class="dropdown-link" href="book_ad_step1.php">Newspaper</a></li>
	                     <hr />
                         <li><a class="dropdown-link" href="online_booking.php">Online</a></li>
	                     <hr />
                         <li><a class="dropdown-link" href="tv_booking.php">Television</a></li>
                        </ul>
                </div>
             
            </li>
            
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://techlabdigital.com/demo/php/agency2/index.php#about">About</a>
            </li>
            
			<?php
			   if(isset($_SESSION["id"])){ 
				 echo' <li class="nav-item">
			    <div class="dropdown">
				    <a class=" nav-link js-scroll-trigger dropdown-toggle"  id="dropdownMenu1" data-toggle="dropdown" data-hover="dropdown">My Profile.
                    <span class="caret"></span></a>
                       <ul class="dropdown-menu">
                         <li><a class="dropdown-link" href="#">Profile</a></li>
	                     <hr />
                         <li><a class="dropdown-link" href="logout.php">Logout</a></li>
	                     <hr />
                         <li><a class="dropdown-link" href="#">History</a></li>
						 <hr />
                         <li><a class="dropdown-link" href="#">Invoice</a></li>
                        </ul>
                </div>
             
            </li>'; 
			   }
			   else{
				   echo '<li class="nav-item">
                         <a class="nav-link js-scroll-trigger" href="http://techlabdigital.com/demo/php/agency2/login.php">Login</a>
                          </li>';
			   }
			?>
            
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="http://techlabdigital.com/demo/php/agency2/index.php#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
	

  	
	 <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	 
	 <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
     
     <!-- login form JavaScript -->
	<script src="js/login.js"></script>  
         
    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
   

       

	</body>

</html>
