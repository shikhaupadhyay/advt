<?php
function currentstep($status,$substatus) 
{
  if ($status == 'PENDING')
        return 1;
    else if ($status == 'APPROVED')
    {
        if ($substatus == 'REVIEW'){
            return 2;
          }  
        if ($substatus == 'AWAITING_PAYMENT'){
            return 3;
          } 
        if ($substatus == 'AD_SCHEDULING'){
            return 4;
          } 
        if ($substatus == 'AD_PUBLISHING'){
            return 5;
          } 
     }
    
    else if (status == 'PUBLISHED'){
        return 6; //last step count
        }
    else if (status == 'REJECTED'){
        return 0;
    }  
}
?>
<html>
    <head>
       <link rel="stylesheet" href="./css/my_area.css">

      
    </head>
    <body>
       <?php
        include("includes/connection.php");
        include("header.php");
         $booking_id=$_POST['booking_id'] ;      
         $sql=mysql_query("SELECT * FROM `ad_booking` where id='".$booking_id."'");
         $result=mysql_fetch_object($sql);
         $booking_status=$result->booking_status;
         $substatus=$result->substatus;
         $complete=currentstep($booking_status,$substatus);
         if(!isset($_SESSION['user_id'])){
	echo "<script>window.location.href='index.php'</script>";
	exit;
        }
         
         
       ?> 
    
       <section>
       
           <div class="container">
             <div class="row" >
                <div class="col-lg-12 light_yellow">
                     Your Ad ID:<?php echo $result->id ?>
                </div>
                
              </div> 
              <input type="hidden" id="step" value="<?php echo $complete ?>"> 
           </div>
          </br>
          <div class="container">
               <div class="row">
                    <div class="col-lg-12">
		           <ul style="list-style-type:none" id="progress">
				    <li class="disabled">
				       <div style="padding:20px;">
				          <div class="dot grey"></div>  
				           <p>Generating Ad Preview</p></br>
				           <p> We are generating your Ad Previews based on the information provided. Our Ad Expert will call you shortly.</p>
				         </div><hr>
				     </li>
				    
				    <li class="disabled">
				       <div style="padding:20px;">
				          <div class="dot grey"></div>  
				           <p>Review Ad And Pay </p></br>
				           <p>Your ad previews will be displayed here. Select an Ad option, review it, and pay for your order.</p>
				         </div><hr>
				    </li>
				    
				    <li class="disabled">
				      <div style="padding:20px;">
				          <div class="dot grey"></div>  
				           <p>Awaiting Payment</p></br>
				           <p>Your payment is being processed or has not been received yet. Please try to resend it.  </p>
				           <a href="#" class="btn btn-primary book-button">Payment</a>
				         </div> <hr>
				   
				    </li>
                                    
				    <li class="disabled">
				    <div style="padding:20px;">
				          <div class="dot grey"></div>  
				           <p>Ad Scheduling </p></br>
				           <p>Your Ad will be scheduled for release on the specified date.</p>
				         </div> <hr>
				    </li>
                                   <li class="disabled">
				    <div style="padding:20px;">
				          <div class="dot grey"></div>  
				           <p>Ad Publishing </p></br>
				           <p>Your Ad will be published. You may view a scanned copy of the Ad after it is published.</p>
				         </div> <hr>
				    </li>
				   
				    
                           </ul>
			   
		    </div> 
		</div>          
	   </div> 

           
        <input type="hidden" id="step" value="<?php echo $complete ?>"> 	 
       </section>
    </body>
    <script type="text/javascript" src="js/my_area.js">

    </script>
  </html> 