<html>
    <head>
    </head>
     <body id="page-top">
  <div class="container">
  <?php 
      include("includes/connection.php");
	  include("header.php");
      $_SESSION["media_id"]=3;
   ?>
   </div>
   
         <!-- select tab -->
	<section id="select"> 
	<div class="row ">
					<div class="col-lg-12 ">
						<div id="error">
							<?php
							if(!isset($_SESSION["id"])){ 
				                    echo '<p class="help-block text-danger container">please login first for start  booking </p>'; 
			                     }
								   
			                ?>
						</div>
					</div>
				</div></br>
   
	                <div class="row">
                        <div class="col-lg-12">
                            <form id="onlineform"  method="post" action= "tv_save.php">
                            <div class="row">
	                            <div class="col-lg-9 container">
					        		<fieldset class="border p-2 form-group">
										<legend class="w-auto">Ad. Content</legend> 
										<div class="form-group">
										    <select required class="form-control" id="channel"  name="channel">
													<option selected="true" disabled="disabled" value="">Select Channel</option>
													<?php 
														$sql ="SELECT channel_id,channel_name FROM television_ad";
														$result=mysql_query($sql);
														print_r($result);
														while($list=mysql_fetch_array($result)){
															 echo '<option value="'.$list['0'].'" name="a">'.$list['1'].'</option>';
														}
											
													?>                           
											</select>
										</div>	
										<div class="form-group">
										    <select required class="form-control" id="type"  name="type">
													<option selected="true" disabled="disabled" value="">Select </option>
													<option  value="Prime">Prime </option>
													<option  value="Reguler">Reguler </option>
													                     
											</select>
										</div>
									    <div class="form-group">
									        <textarea class="form-control" id="message" placeholder="Ad Content*" required="required" data-validation-required-message="Please enter Your content." name="message"></textarea>
									        <p class="help-block text-danger"></p>
									    </div>
									</fieldset>	
									<fieldset class="border p-2 form-group">
										<legend class="w-auto">Personal Details</legend> 
										<div class="form-group"  >
										    <input class="form-control" name="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">	
											<p class="help-block text-danger"></p>
										</div>
										<div class="form-group"  >
											 <input class="form-control" name="email" type="email" placeholder="Your Email *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required="required" data-validation-required-message="Please enter your email address.">
											 <p class="help-block text-danger"></p>
										</div>
										<div class="form-group" >
										     <input class="form-control" name="phone" type="tel" placeholder="Your Phone *" pattern="[789][0-9]{9}" required="required" data-validation-required-message="Please enter your phone number.">
											 <p class="help-block text-danger"></p>
										</div>
									</fieldset>	
									
					 
                                </div>
                            </div>
                        <div class="clearfix"></div>
						<div class="col-lg-12 text-center">
						  <div id="success"></div>
						  <button id="booking_submit" class="btn btn-primary btn-xl text-uppercase" type="submit" <?php if (!isset($_SESSION["id"])){ ?> disabled <?php } ?> >Book Ad</button>
						  
						</div>
                    </div>
            </form>
          </div>

    </section>
	
       
    <script src="js/booking.js"></script>
	<?php
             include("footer.php");
     ?> 
  </body>
  
</html>