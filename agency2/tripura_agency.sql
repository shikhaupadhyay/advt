-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2018 at 03:19 PM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tripura_agency`
--

-- --------------------------------------------------------

--
-- Table structure for table `ad_booking`
--

CREATE TABLE `ad_booking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `booking_state` varchar(30) NOT NULL,
  `payment_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad_booking`
--

INSERT INTO `ad_booking` (`id`, `user_id`, `media_id`, `invoice_id`, `booking_state`, `payment_status`) VALUES
(1, 16, 2, 49, 'pending', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ad_category`
--

CREATE TABLE `ad_category` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad_category`
--

INSERT INTO `ad_category` (`id`, `name`) VALUES
(1, 'Business'),
(2, 'Computers'),
(3, 'Display'),
(4, 'Education');

-- --------------------------------------------------------

--
-- Table structure for table `ad_subcategory`
--

CREATE TABLE `ad_subcategory` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad_subcategory`
--

INSERT INTO `ad_subcategory` (`id`, `category_id`, `name`) VALUES
(1, 1, 'Agent'),
(2, 1, 'Business Center '),
(3, 1, 'Catring'),
(4, 2, 'Computer Education'),
(5, 2, 'Dotcoms'),
(6, 2, 'Forum Computer Shope'),
(7, 2, 'Hardware'),
(8, 2, 'Software'),
(9, 3, 'Commercial'),
(10, 3, 'Governement'),
(11, 3, 'Retail'),
(12, 4, 'Account'),
(13, 4, 'Admission'),
(14, 4, 'Banking'),
(15, 4, 'Civil Service');

-- --------------------------------------------------------

--
-- Table structure for table `ad_type`
--

CREATE TABLE `ad_type` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `type_image` varchar(80) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ad_type`
--

INSERT INTO `ad_type` (`id`, `type`, `type_image`, `name`) VALUES
(1, 'text', 'img/sample-classified-text-ad-big.jpg', 'Classified Text Ad'),
(2, 'semi_display', 'img/sample-classified-display-ad-big.jpg', 'Classified Display Ad'),
(3, 'display', 'img/sample-display-ad-big.jpg', 'Display Ad');

-- --------------------------------------------------------

--
-- Table structure for table `edition_city_map`
--

CREATE TABLE `edition_city_map` (
  `id` int(11) NOT NULL,
  `newspaper_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `city` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edition_city_map`
--

INSERT INTO `edition_city_map` (`id`, `newspaper_id`, `editor_id`, `city`) VALUES
(1, 1, 1, 'Noida'),
(2, 1, 2, 'Gurgaon');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `cost` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `cost`) VALUES
(1, 0),
(2, 200),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 200),
(9, 200),
(10, 200),
(11, 0),
(12, 200),
(13, 200),
(14, 200),
(15, 200),
(16, 200),
(17, 200),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 200),
(28, 0),
(29, 0),
(30, 100),
(31, 200),
(32, 200),
(33, 0),
(34, 200),
(35, 200),
(36, 200),
(37, 100),
(38, 100),
(39, 100),
(40, 100),
(41, 100),
(42, 100),
(43, 100),
(44, 100),
(45, 100),
(46, 100),
(47, 100),
(48, 100),
(49, 100);

-- --------------------------------------------------------

--
-- Table structure for table `media_category`
--

CREATE TABLE `media_category` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_category`
--

INSERT INTO `media_category` (`id`, `name`) VALUES
(1, 'Newspaper'),
(2, 'Online'),
(3, 'Television');

-- --------------------------------------------------------

--
-- Table structure for table `newspaper_ad`
--

CREATE TABLE `newspaper_ad` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `logo` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newspaper_ad`
--

INSERT INTO `newspaper_ad` (`id`, `name`, `logo`) VALUES
(1, 'The Times of India', ''),
(2, 'Dainik Bhaskar', ''),
(3, 'Dainik Jagran ', ''),
(4, 'Amar Ujala', '');

-- --------------------------------------------------------

--
-- Table structure for table `newspaper_ad_city`
--

CREATE TABLE `newspaper_ad_city` (
  `id` int(11) NOT NULL,
  `state` varchar(30) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city` varchar(30) NOT NULL,
  `city_id` int(11) NOT NULL,
  `newspaper_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newspaper_ad_city`
--

INSERT INTO `newspaper_ad_city` (`id`, `state`, `state_id`, `city`, `city_id`, `newspaper_id`) VALUES
(1, 'Utter Pradesh', 1, 'Noida', 101, 1),
(2, 'Utter Pradesh', 1, 'Lucknow', 102, 1),
(3, 'Delhi', 2, 'Delhi', 202, 2),
(4, 'Delhi', 2, 'Noida', 202, 2);

-- --------------------------------------------------------

--
-- Table structure for table `newspaper_ad_detail`
--

CREATE TABLE `newspaper_ad_detail` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `newspaper_id` int(11) NOT NULL,
  `editon_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `content` varchar(500) NOT NULL,
  `rate` double NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newspaper_ad_edition`
--

CREATE TABLE `newspaper_ad_edition` (
  `id` int(11) NOT NULL,
  `newspaper_id` int(11) NOT NULL,
  `edition` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newspaper_ad_edition`
--

INSERT INTO `newspaper_ad_edition` (`id`, `newspaper_id`, `edition`) VALUES
(1, 1, 'Delhi'),
(2, 1, 'Gorakhpur'),
(3, 2, 'Delhi'),
(4, 2, 'Gorakhpur'),
(5, 3, 'Delhi'),
(6, 3, 'Gorakhpur'),
(7, 4, 'Delhi'),
(8, 4, 'Gorakhpur');

-- --------------------------------------------------------

--
-- Table structure for table `newspaper_rate`
--

CREATE TABLE `newspaper_rate` (
  `newspaper_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newspaper_rate`
--

INSERT INTO `newspaper_rate` (`newspaper_id`, `editor_id`, `rate`) VALUES
(1, 1, 200),
(1, 2, 100),
(2, 3, 150),
(2, 4, 250),
(3, 5, 170),
(3, 6, 140),
(4, 7, 230),
(4, 8, 130);

-- --------------------------------------------------------

--
-- Table structure for table `online_ad`
--

CREATE TABLE `online_ad` (
  `id` int(11) NOT NULL,
  `web_name` varchar(30) NOT NULL,
  `web_url` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_ad`
--

INSERT INTO `online_ad` (`id`, `web_name`, `web_url`) VALUES
(1, 'Techlab Digital', 'http://techlabdigital.com/'),
(2, 'Youtube', 'https://www.youtube.com/'),
(3, 'Google', 'https://www.google.com/');

-- --------------------------------------------------------

--
-- Table structure for table `online_ad_detail`
--

CREATE TABLE `online_ad_detail` (
  `id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `web_id` int(11) NOT NULL,
  `content` varchar(500) NOT NULL,
  `rate` double NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_ad_detail`
--

INSERT INTO `online_ad_detail` (`id`, `ad_id`, `web_id`, `content`, `rate`, `name`, `email`, `phone`) VALUES
(1, 1, 1, 'sdfksdfh sdkfjsdkfj lksdjf sdklf', 100, 'shikha', 'shikha@techlabdigital.com', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `online_rate`
--

CREATE TABLE `online_rate` (
  `id` int(11) NOT NULL,
  `web_id` int(11) NOT NULL,
  `rate` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_rate`
--

INSERT INTO `online_rate` (`id`, `web_id`, `rate`) VALUES
(1, 1, 100),
(2, 2, 200),
(3, 3, 150);

-- --------------------------------------------------------

--
-- Table structure for table `television_ad`
--

CREATE TABLE `television_ad` (
  `channel_id` int(11) NOT NULL,
  `channel_name` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `television_rate`
--

CREATE TABLE `television_rate` (
  `id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `prime_time_rate` double NOT NULL,
  `regular_rate` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(8) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `phone` int(11) NOT NULL,
  `auth_flag` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `phone`, `auth_flag`) VALUES
(17, 'f@gmail.com', '', 'abc', 'def', 0, 0),
(16, 'a@gmail.com', '123', 'shikha', 'upadhyay', 0, 0),
(15, '', '', '', '', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad_booking`
--
ALTER TABLE `ad_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_category`
--
ALTER TABLE `ad_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_subcategory`
--
ALTER TABLE `ad_subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_type`
--
ALTER TABLE `ad_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edition_city_map`
--
ALTER TABLE `edition_city_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_category`
--
ALTER TABLE `media_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newspaper_ad`
--
ALTER TABLE `newspaper_ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newspaper_ad_city`
--
ALTER TABLE `newspaper_ad_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newspaper_ad_detail`
--
ALTER TABLE `newspaper_ad_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newspaper_ad_edition`
--
ALTER TABLE `newspaper_ad_edition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_ad`
--
ALTER TABLE `online_ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_ad_detail`
--
ALTER TABLE `online_ad_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_rate`
--
ALTER TABLE `online_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `television_ad`
--
ALTER TABLE `television_ad`
  ADD PRIMARY KEY (`channel_id`);

--
-- Indexes for table `television_rate`
--
ALTER TABLE `television_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad_booking`
--
ALTER TABLE `ad_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ad_category`
--
ALTER TABLE `ad_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ad_subcategory`
--
ALTER TABLE `ad_subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ad_type`
--
ALTER TABLE `ad_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `edition_city_map`
--
ALTER TABLE `edition_city_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `media_category`
--
ALTER TABLE `media_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `newspaper_ad`
--
ALTER TABLE `newspaper_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `newspaper_ad_city`
--
ALTER TABLE `newspaper_ad_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `newspaper_ad_detail`
--
ALTER TABLE `newspaper_ad_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newspaper_ad_edition`
--
ALTER TABLE `newspaper_ad_edition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `online_ad`
--
ALTER TABLE `online_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `online_ad_detail`
--
ALTER TABLE `online_ad_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `online_rate`
--
ALTER TABLE `online_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `television_ad`
--
ALTER TABLE `television_ad`
  MODIFY `channel_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `television_rate`
--
ALTER TABLE `television_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
